(defun rl-mark-range (start end)
  "mark text from start to end"
  (setq deactivate-mark nil)
  (set-mark (min start end))
  (goto-char (max start end))
  (activate-mark)
  )

(defun rl-mark-lines (start end)
  "Mark lines containing start to end"
  (let (bufmin bufmax)
    (goto-char (min start end))
    (setq bufmin (line-beginning-position))
    (goto-char (max start end))
    (setq bufmax (line-end-position))
    (rl-mark-range bufmin bufmax)
    )
  )

(defun rl-mark-lines-by-number(start end)
  "Mark lines by line number"
  (let (min-pos max-pos)

    (goto-line start)
    (setq min-pos (line-beginning-position))
    (goto-line end)
    (setq max-pos (line-end-position))
    (rl-mark-lines min-pos max-pos)
  )
)

(provide 'rl-mark)
