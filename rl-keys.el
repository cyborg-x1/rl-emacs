(require 'rl-cmake-ide)
(require 'rl-move)
(require 'rl-delete)
(require 'rl-menu)

(defun rl-init-editing-keys-modes()
  "Overwrites mode keys when in list"
  (loop for x in '(c++-mode-hook)
        do
        (add-hook
         x
         (lambda ()
           (local-set-key (kbd "C-d") #'rl-delete-row)
           (local-set-key (kbd "M-<up>") #'rl-move-up)
           (local-set-key (kbd "M-<down>") #'rl-move-down)
           (local-set-key (kbd "C-v") #'yank)
           (local-set-key (kbd "C-SPC") #'rl-menu)
           )
         )
        )
  )




;; Init rl-keys
(defun rl-keys-init()
  (rl-init-editing-keys-modes)
  (global-set-key (kbd "C-b") 'rl-cmake-compile)
  (global-set-key (kbd "C-d") 'rl-delete-row)
  (global-set-key (kbd "M-<up>") 'rl-move-up)
  (global-set-key (kbd "M-<down>") 'rl-move-down)
  (global-set-key (kbd "C-v") 'yank)
  (global-set-key (kbd "C-SPC") 'rl-menu)
  )

(provide 'rl-keys)
