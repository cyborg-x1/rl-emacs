(require 'popup)
(require 'menu-bar)


(defun rl-menu()
  "launches rl menu"
  (interactive)
  (let (
        (choice (popup-menu* '(
                               "Autocomplete"
                               "Yasnippet"
                               )))
       )
    (if (string= choice  "Autocomplete") (auto-complete))
    (if (string= choice  "Yasnippet") (yas-insert-snippet))
  )
)


(provide 'rl-menu)
