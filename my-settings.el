
(require 'rl-cmake-ide)
(require 'rl-keys)

(defun rl-apply-settings()
  (rl-keys-init)
  (rl-cmake-ide-init)
)

(provide 'my-settings)
